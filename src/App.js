import React, { Component } from 'react';
import './App.css';
import {Route, Switch} from 'react-router-dom';
import getParamFromUrl from './helpers/urlParams';

import UserDefaults from './services/userDefaults'

import HeaderContainer from "./containers/HeaderContainer";
import TopicsScreen from "./containers/TopicsScreen";
import Login from "./containers/Login";
import AddTopic from "./containers/AddTopic";
import Reply from "./containers/Reply";
import Edit from "./containers/Edit";
import TopicScreen from "./containers/TopicScreen";
import ModalError from "./containers/ModalError";
import Help from "./components/Help";
import Logout from "./containers/Logout";

class App extends Component {

    render() {
        return (
            <div className='App'>
                <HeaderContainer />
                <ModalError/>
                <Switch>
                    <Route path='/' exact component={TopicsScreen} />
                    <Route path='/help' exact component={Help}/>
                    <Route path='/login' exact component={Login} />
                    <Route path='/logout' exact component={Logout} />
                    <Route path='/add-topic' exact component={AddTopic} />
                    <Route path='/reply/:id' exact component={Reply} />
                    <Route path='/edit/:id' exact component={Edit} />
                    <Route path='/topic/:id' exact component={TopicScreen} />
                </Switch>
            </div>
        );
    }
}

export default App;
