

export const objectByPath = function(o, s) {
    if (s === "") {
        return o;
    }
    const {object, key} = findObjectAndKeyByPath(o, s)
    return object[key]
}

export const findObjectAndKeyByPath = function (o, s) {
    var key = s
    var object = o
    if (s === "") {
        return {key, object};
    }
    s = s.replace(/\[(\w+)\]/g, '.$1'); // convert indexes to properties
    s = s.replace(/^\./, '');           // strip a leading dot
    var a = s.split('.');
    for (var i = 0; i < a.length; ++i) {
        var k = a[i];
        if (k in o) {
            object = o
            key = k
            o = o[k];
        } else {
            return;
        }
    }
    return {key, object};
}

export const removeObjectByPath = function(o, s) {
    const {object, key} = findObjectAndKeyByPath(o, s)
    if (Array.isArray(object)) {
        object.splice(key, 1);
    } else {
        delete object[key];
    }
}

export const objectMerge = function(oTo, oFrom) {
    Object.keys(oFrom).forEach(key => {
        oTo[key] = oFrom[key];
    });
}