
export default function getParamFromUrl(paramName) {
    const url = new URL(window.location);
    const params = new URLSearchParams(url.search);
    return params.get(paramName);
}