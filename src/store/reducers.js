import node from './node/reducer';
import common from './common/reducer';
import account from './account/reducer';
import { applyMiddleware, combineReducers } from 'redux';
import thunk from 'redux-thunk';
import {routerReducer} from "react-router-redux";

export const reducers = combineReducers({
    ...{
        common,
        node,
        account
    }, routing: routerReducer});