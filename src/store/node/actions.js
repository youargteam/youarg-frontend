import _ from "lodash"
import server from '../../services/server'
import * as actionTypes from './actionTypes'
import * as commonActionTypes from '../common/actionTypes'

export function getParents(page) {
    return dispatch => {
        server.getParents(page).then(response => {
            dispatch({type: actionTypes.PARENTS_FETCHED, data: response.data, page: page});
        });
    };
}

export function getTree(nodePK, page = 1, dictionaryPath = "") {
    return dispatch => {
        server.getNode(nodePK).then(response => {
            var parentNode = response.data
            server.getTree(nodePK, page).then(response => {
                var tree = response.data
                tree.nextPage = 2
                parentNode.children = tree
                dispatch({type: actionTypes.TREE_FETCHED, tree: parentNode, dictionaryPath})
            });
        })

    };
}

export function getNode(nodePK) {
    return dispatch => {
        server.getNode(nodePK).then(response => {
            var node = response.data
            dispatch({type: actionTypes.NODE_FETCHED, node: node})
        })
    }
}

export function createNode(text, parent, argument) {
    return dispatch => {
        server.createNode(text, parent, argument).then(response => {
            if (response.data !== undefined) {
                dispatch({type: actionTypes.NODE_CREATED, createdNode: response.data});
            }
        }).catch(reason => showError(reason.dispatch));
    };
}

export function editNode(pk, text, parent, argument) {
    return dispatch => {
        server.editNode(pk, text, parent, argument).then(response => {
            if (response.data !== undefined) {
                dispatch({type: actionTypes.NODE_EDITED, editedNode: response.data});
            }
        }).catch(reason => showError(reason, dispatch));
    };
}

export function removeNode(pk, path = "") {
    return dispatch => {
        server.removeNode(pk).then(response => {
            if (response.data !== undefined) {
                dispatch({type: actionTypes.NODE_REMOVED, path});
            }
        }).catch(reason => showError(reason, dispatch));
    };
}

export function setImportance(node, importance, path) {
    return dispatch => {
        var serverMethod = server.setImportance.bind(server)
        if (node.my_importance !== null) {
            serverMethod = server.updateImportance.bind(server)
        }
        serverMethod(node, importance).then(response => {
            dispatch({type: actionTypes.IMPORTANCE_SET, newNode: response.data.node, path: path})
        }).catch(reason => showError(reason, dispatch));
    }
}

export function deleteImportance(node, path) {
    return dispatch => {
        server.deleteImportance(node.my_importance.pk).then(response => {
            server.getNode(node.pk).then(response => {
                dispatch({type: actionTypes.IMPORTANCE_SET, newNode: response.data, path: path})
            })
        })
    }
}

function showError(reason, dispatch) {
    const messages = _.values(reason.response.data);
    const message = messages[0];
    dispatch({type: commonActionTypes.SERVER_ERROR, message})
}