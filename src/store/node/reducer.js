// reducers hold the store's state (the initialState object defines it)
// reducers also handle plain object actionTypes and modify their state (immutably) accordingly
// this is the only way to change the store's state
// the other exports in this file are selectors, which is business logic that digests parts of the store's state
// for easier consumption by views

import Immutable from 'seamless-immutable';
import * as types from './actionTypes';
import {objectByPath, objectMerge, removeObjectByPath} from '../../helpers/objectExtentions';
import _ from 'lodash';

const initialState = Immutable({
    parents: [],
    parentsNextPage: 1,
    hasNextParents: true,
    tree: {},
    createdNode: undefined
});

export default function reduce(state = initialState, action = {}) {
    var tree = undefined
    switch (action.type) {
        case types.PARENTS_FETCHED:
            const parents = action.page === 1 ? [] : state.parents // clear parents when reload
            return state.merge({
                parents: parents.concat(action.data.results),
                hasNextParents: action.data.next !== null,
                parentsNextPage: action.page + 1
            });
        case types.TREE_FETCHED:
            tree = setupTree(state.tree, action);
            return state.merge({
                tree: tree
            });
        case types.NODE_FETCHED:
            return state.merge({
                node: action.node
            });
        case types.NODE_CREATED:
            return state.merge({
                createdNode: action.createdNode
            })
        case types.NODE_EDITED:
            return state.merge({
                createdNode: action.editedNode
            })
        case types.NODE_REMOVED:
            tree = deleteNode(state.tree, action.path)
            return state.merge({
                tree: tree
            })
        case types.IMPORTANCE_SET:
            tree = updateTree(state.tree, action.newNode, action.path)
            return state.merge({
                tree: tree
            })
        case types.CLEAR_NODE:
            return state.merge({
                node: initialState.node
            })
        case types.CLEAR_NODE_CREATED:
            return state.merge({
                createdNode: initialState.createdNode
            })
        case types.CLEAR_PARENTS:
            return state.merge({
                parents: initialState.parents
            })
        case types.CLEAR_TREE:
            return state.merge({
                tree: initialState.tree
            })
        default:
            return state;
    }
}

function setupTree(oldTree, action) {
    var oldTree = _.cloneDeep(oldTree)
    const subTree = action.tree
    const dictionaryPath = action.dictionaryPath
    var oldSubTree = objectByPath(oldTree, dictionaryPath)
    var oldSubTreeChildren = oldSubTree.children

    // if it is the first call for the subtree
    if (oldSubTreeChildren === undefined || Object.values(oldSubTreeChildren).length === 0) {
        objectMerge(oldSubTree, subTree)
        return oldTree
    }

    objectMerge(oldSubTree, subTree) //update the node
    oldSubTree.children = oldSubTreeChildren //but restore children

    // if it is the first time children are added or it just was preview, just put children, else combine with existent children
    if (oldSubTreeChildren.nextPage === 1 || oldSubTreeChildren.nextPage === undefined) {
        oldSubTreeChildren.nextPage = 2
        oldSubTreeChildren.results = subTree.children.results
    } else {
        Array.prototype.push.apply(oldSubTreeChildren.results, subTree.children.results)
        oldSubTreeChildren.nextPage += 1
    }

    oldSubTreeChildren.count = subTree.count // update the count just in case some nodes were added

    return oldTree
}

function deleteNode(oldTree, path) {
    if (path === "") {
        return undefined
    }
    var oldTree = _.cloneDeep(oldTree)
    removeObjectByPath(oldTree, path)
    return oldTree
}

function updateTree(oldTree, newNode, path) {
    var oldTree = _.cloneDeep(oldTree)
    var oldSubTree = objectByPath(oldTree, path)
    Object.assign(oldSubTree, newNode) // merge old and new node
    return oldTree
}

export function getParents(state) {
    return state.node.parents;
}

export function getParentsNextPage(state) {
    return state.node.parentsNextPage;
}

export function getHasMoreParents(state) {
    return state.node.hasNextParents;
}

export function getTree(state) {
    return state.node.tree;
}

export function getNode(state) {
    return state.node.node;
}

export function getCreatedNode(state) {
    return state.node.createdNode;
}