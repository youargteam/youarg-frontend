// reducers hold the store's state (the initialState object defines it)
// reducers also handle plain object actionTypes and modify their state (immutably) accordingly
// this is the only way to change the store's state
// the other exports in this file are selectors, which is business logic that digests parts of the store's state
// for easier consumption by views

import Immutable from 'seamless-immutable';
import * as types from './actionTypes';

const initialState = Immutable({
    error: {
        message: undefined
    }
});

export default function reduce(state = initialState, action = {}) {
    switch (action.type) {
        case types.SERVER_ERROR:
            return state.merge({
                error: {
                    message: action.message
                }
            });
        default:
            return state;
    }
}

export function getError(state) {
    return state.common.error;
}