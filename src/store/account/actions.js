import server from '../../services/server'
import * as actionTypes from './actionTypes'

export function getSelfAccount() {
    return dispatch => {
        server.getSelfAccount().then(response => {
            dispatch({type: actionTypes.SELF_ACCOUNT_FETCHED, account: response.data})
        }).catch(reason => {
            dispatch({type: actionTypes.SELF_ACCOUNT_FETCHED, account: undefined})
        })
    }
}