import * as actionTypes from './actionTypes'
import Immutable from 'seamless-immutable'

const initialState = Immutable({
    selfAccount: undefined
})

export default function reduce(state = initialState, action = {}) {
    switch (action.type) {
        case actionTypes.SELF_ACCOUNT_FETCHED:
            return state.merge({
                selfAccount: action.account
            })
        default:
            return state
    }
}

export function getSelfAccount(state) {
    return state.account.selfAccount
}