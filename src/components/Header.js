import React, {Component} from 'react'
import {FormControl, Nav, Navbar, NavItem, Row, Col} from "react-bootstrap";
import {Link} from "react-router-dom";
import {LinkContainer} from "react-router-bootstrap";

class Header extends Component{
    state = {
        searchInput: '',
        userLogin: false
    };

    searchHandler = e => {
        this.setState({searchInput: e.target.value})
    };

    render() {
        return (
            <Navbar collapseOnSelect>
                <Navbar.Header>
                    <Navbar.Brand>
                        <Link to={'/'}>YouArg</Link>
                    </Navbar.Brand>
                    <Navbar.Toggle/>
                </Navbar.Header>
                <Navbar.Collapse>
                    {/*<Nav>*/}
                    {/*<LinkContainer to={'/tags'}>*/}
                    {/*<NavItem>*/}
                    {/*Тэги*/}
                    {/*</NavItem>*/}
                    {/*</LinkContainer>*/}
                    {/*</Nav>*/}
                    {/*<Navbar.Form pullLeft>*/}
                    {/*<FormControl*/}
                    {/*type='text'*/}
                    {/*value={this.state.searchInput}*/}
                    {/*placeholder='Поиск ноды'*/}
                    {/*onChange={this.searchHandler}*/}
                    {/*/>*/}
                    {/*</Navbar.Form>*/}
                    <Nav pullRight>
                        <LinkContainer to={'/help'}>
                            <NavItem>
                                Помогите!
                            </NavItem>
                        </LinkContainer>
                    </Nav>
                    {this.props.selfAccount === undefined ?
                        <Nav pullRight key="login">
                            <LinkContainer to={'/login'}>
                                <NavItem>
                                    Вход
                                </NavItem>
                            </LinkContainer>
                        </Nav>
                        : [
                            <Nav pullRight key="logout">
                                <LinkContainer to={'/logout'}>
                                    <NavItem>
                                        Выход
                                    </NavItem>
                                </LinkContainer>
                            </Nav>,
                            <Nav pullRight key="user">
                                <NavItem><span className="user-label">{this.props.selfAccount.first_name}</span></NavItem>
                            </Nav>
                        ]
                    }
                </Navbar.Collapse>
            </Navbar>
        );
    }
}

export default Header