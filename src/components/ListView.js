import React, { Component } from 'react';

export default class ListView extends Component {

    render() {
        return (
            <ul className={this.props.noPadding ? "ul-nopadding" : null} style={{listStyle: "none"}}>
                {this.props.rows.map(this.renderRowById.bind(this))}
            </ul>
        );
    }

    renderRowById(row) {
        return (
            <li key={row.pk}>
                {this.props.renderRow(row)}
            </li>
        );
    }

}