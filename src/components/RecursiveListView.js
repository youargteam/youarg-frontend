import ListView from './ListView';
import React from 'react';
import { objectByPath } from "../helpers/objectExtentions";

export default class RecursiveListView extends ListView {

    render() {
        return this.renderRecursively(this.props.tree, "")
    }

    renderRecursively(tree, path) {
        var rows;
        path = `${path}.${this.props.childrenAttributePath}`;
        try {
            rows = objectByPath(tree, this.props.childrenAttributePath);
        } catch (e) {
            return null
        }
        if (rows === undefined) {
            return null
        }

        if (rows.length > 0) {
            return this.renderChildren(rows, path)
        }
        return null
    }

    renderChildren(rows, path) {
        return (
            <ListView
                rows={rows}
                renderRow={(row) => {
                    const rowPath = `${path}[${rows.indexOf(row)}]`;
                    return (
                        <div>
                            {this.props.renderRow(row, rowPath)}
                            <div className="children">
                                {
                                    this.renderRecursively(row, rowPath)
                                }
                            </div>
                        </div>
                    )
                }}
            />
        )
    }
}