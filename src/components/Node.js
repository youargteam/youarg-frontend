import React, { Component } from 'react';
import { Row, Col, Button, DropdownButton, MenuItem } from 'react-bootstrap';
import { withRouter } from 'react-router-dom';

const NodeImportanceButton = (props) => {
    return (
        <Button
            className={(props.active ? "btn-primary " : "") + "small-margins"}
            onClick={props.onButtonClick}>
            <i className="fas fa-exclamation"/>
        </Button>
    )
}

class NodeImportanceGroup extends Component {
    LowValue = 5
    MidValue = 50
    HighValue = 100

    componentWillMount() {
        this.updateState(this.props.importance)
    }

    componentWillReceiveProps(nextProps) {
        this.updateState(nextProps.importance)
    }

    updateState(importance) {
        if (importance === null) {
            this.setState({
                value: -1,
                lowActive: false,
                midActive: false,
                highActive: false,
            })
            return
        }
        const value = importance.value
        this.setState ({
            value,
            lowActive: value >= this.LowValue,
            midActive: value >= this.MidValue,
            highActive: value >= this.HighValue
        })
    }

    onButtonClick (value) {
        if (this.state.value === value) {
            this.props.deleteImportance()
        } else {
            this.props.setImportance(value)
        }
    }

    render() {
        return (
            <div>
                <NodeImportanceButton active={this.state.lowActive} onButtonClick={() => {this.onButtonClick(this.LowValue)}}/>
                <NodeImportanceButton active={this.state.midActive} onButtonClick={() => {this.onButtonClick(this.MidValue)}}/>
                <NodeImportanceButton active={this.state.highActive} onButtonClick={() => {this.onButtonClick(this.HighValue)}}/>
            </div>
        )
    }
}

const NodeImportance = (props) => {
    var importance = props.node.importance_amount === 0 ? 100
        : props.node.importance_sum / props.node.importance_amount
    return <div className="center-vertically">{importance}% важность</div>
}

class Node extends Component {

    render() {
        return (
            <div className='container node-container'>
                <div className="panel panel-default medium-margins">
                    <div className="panel-heading">
                        <Row>
                            <Col md={1}>
                                <DropdownButton title="" id="Опции">
                                    {this.props.selfAccount !== undefined ? (this.props.selfAccount.pk === this.props.node.user.pk ?
                                        <div>
                                            <MenuItem className="large-margins" onSelect={this.props.editPressed}>Редактировать</MenuItem>
                                            <MenuItem className="large-margins" onSelect={this.props.removePressed}>Удалить</MenuItem>
                                        </div>
                                        : null) : null
                                    }
                                </DropdownButton>
                            </Col>
                            <Col md={3}>
                                <div className='container-fluid'>
                                    {this.props.node.created}
                                </div>
                            </Col>
                            <Col sm={4}>
                                <div className='container-fluid'>
                                    {this.props.node.truthfulness + "% правда"}
                                </div>
                            </Col>
                            <Col md={4}>
                                <div className='container-fluid'>
                                    {`${this.props.node.user.first_name}(${this.props.node.user.karma_cached})`}
                                </div>
                            </Col>
                        </Row>
                    </div>
                    <div
                        className={
                            "panel-body " +
                            (this.props.isParent ? "node-body" :
                                (this.props.node.argument ? "node-body-argument" : "node-body-counterargument"))
                        }>
                        {this.props.node.text}
                    </div>
                    {this.props.hideFooter ? null :
                        <div className="panel-footer">
                            <Row>
                                <Col md={4}>
                                    {(this.props.isParent && !this.props.treeMode) || !this.props.showReplyButton ? null
                                        : <Button className="btn btn-primary"
                                                  onClick={() => this.props.history.push(`/reply/${this.props.node.pk}`)}>Ответить</Button>
                                    }
                                </Col>
                                <Col md={4}>
                                    {this.props.treeMode ? null : (
                                        this.props.isParent
                                            ? <Button className="btn btn-primary"
                                                      href={`/topic/${this.props.node.pk}`}>Зайти</Button>
                                            : <NodeImportance node={this.props.node}/>)
                                    }
                                </Col>
                                <Col>
                                    {this.props.isParent ? null
                                        : <NodeImportanceGroup
                                            importance={this.props.node.my_importance}
                                            setImportance={(value) => {
                                                this.props.setImportance(this.props.node, value, this.props.path)
                                            }}
                                            deleteImportance={() => {
                                                this.props.deleteImportance(this.props.node, this.props.path)
                                            }}/>
                                    }
                                </Col>
                            </Row>
                        </div>
                    }
                </div>
            </div>
        )
    }
}

export default withRouter(Node)