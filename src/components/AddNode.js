import React, {Component} from 'react';
import {Row, Col, Button} from 'react-bootstrap';
import SimpleSchema from 'simpl-schema';
import AutoForm from 'uniforms-bootstrap3/AutoForm';
import AutoField from 'uniforms-bootstrap3/AutoField';
import RadioField from 'uniforms-bootstrap3/RadioField';
import SubmitField from 'uniforms-bootstrap3/SubmitField';
import {withRouter} from 'react-router-dom';

import {TopicHints, AnswerHints} from "./Help";

const AddNodeSchema = new SimpleSchema({
    isArgument: {
        type: String,
        label: null,
        allowedValues: ['Аргумент', 'Контраргумент']
    },
    text: {
        type: String,
        label: null
    },
})

class AddNode extends Component {

    onValidSubmit(values) {
        this.props.createNode(
            values.text,
            this.props.parent,
            values.isArgument === 'Аргумент'
        )
    }

    getModel() {
        var model = {
            isArgument: this.props.isArgument !== undefined ?
                (this.props.isArgument ? 'Аргумент' : 'Контраргумент')
                : null,
            text: this.props.text
        }
        return Object.assign(model, this.props.isTopic ? {isArgument: 'Аргумент'} : {})
    }

    render () {
        return (
            <div>
                <div className='panel panel-default panel-medium-margins center-horizontally container-medium large-margins'>
                    <div className='panel-heading'>
                        <h3>{this.props.title}</h3>
                    </div>
                    <div className='panel-body large-margins'>
                        <AutoForm
                            schema={AddNodeSchema}
                            onSubmit={this.onValidSubmit.bind(this)}
                            model={this.getModel()}>
                            {this.props.isTopic ? null : <RadioField name='isArgument'/>}
                            <AutoField name='text' placeholder='Текст'/>
                            <Row>
                                <Col md={4}/>
                                <Col md={2}>
                                    <Button onClick={() => this.props.history.goBack()}>Отменить</Button>
                                </Col>
                                <Col md={2}>
                                    <SubmitField value='Отправить'/>
                                </Col>
                            </Row>
                        </AutoForm>
                    </div>
                </div>
                {this.props.isTopic ? <TopicHints/> : <AnswerHints/>}
            </div>
        );
    }
}

export default withRouter(AddNode)