import server from './server';

export default class UserDefaults {
    static setToken(token) {
        localStorage.setItem('token', token);
        this.updateServer();
    }

    static clearToken() {
        localStorage.removeItem('token');
        this.updateServer();
    }

    static updateServer() {
        server.setToken(this.getToken());
    }

    static getToken() {
        return localStorage.getItem('token');
    }
}