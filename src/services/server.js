import axios from 'axios';

export const serverBaseURL = 'https://youarg-backend.herokuapp.com/';

class Server {

    constructor() {
        this.axios = axios.create({
            baseURL: serverBaseURL,
            timeout: 5000,
            headers: {}
        });
        this.setToken(localStorage.getItem('token'));
    }

    setToken(token) {
        if (token !== undefined) {
            this.axios.defaults.headers.common['Authorization'] = token ? `Token ${token}` : '';
        }
    }

    getParents(page) {
        return this.axios.get(`/node/parents/?page=${page}`);
    }

    getTree(nodeID, page) {
        return this.axios.get(`/node/tree/${nodeID}/?page=${page}`);
    }

    getNode(nodeID) {
        return this.axios.get(`/node/${nodeID}/`);
    }

    createNode(text, parent, argument) {
        const params = {text, parent, argument};
        return this.axios.post('/node/', params);
    }

    editNode(pk, text, parent, argument) {
        const params = {text, parent, argument};
        return this.axios.patch(`/node/${pk}/`, params);
    }

    removeNode(pk) {
        return this.axios.delete(`/node/${pk}/`);
    }

    setImportance(node, importance) {
        const params = {
            node_pk: node.pk,
            value: importance
        }
        return this.axios.post('/node/importance/', params);
    }

    updateImportance(node, importance) {
        const params = {
            value: importance
        }
        return this.axios.patch(`/node/importance/${node.my_importance.pk}/`, params);
    }

    deleteImportance(nodeID) {
        return this.axios.delete(`/node/importance/${nodeID}/`);
    }

    getSelfAccount() {
        return this.axios.get('/user/self')
    }
}

const server = new Server();

export default server;