import React, {Component} from 'react'
import {connect} from 'react-redux'
import Node from '../components/Node'
import * as accountSelectors from '../store/account/reducer'
import * as nodeActions from '../store/node/actions'
import { withRouter } from 'react-router-dom';

class NodeWrapper extends Component {

    editPressed(e) {
        this.props.history.push(`/edit/${this.props.node.pk}`)
    }

    removePressed(e) {
        this.props.dispatch(nodeActions.removeNode(this.props.node.pk, this.props.path))
    }

    render() {
        return <Node {...this.props}
                     editPressed={this.editPressed.bind(this)}
                     removePressed={this.removePressed.bind(this)}
                     showReplyButton={this.props.selfAccount !== undefined}
        />
    }
}

function mapStateToProps(state) {
    return {
        selfAccount: accountSelectors.getSelfAccount(state)
    }
}

export default withRouter(connect(mapStateToProps)(NodeWrapper))