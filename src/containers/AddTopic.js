import React, {Component} from 'react';
import { Redirect } from 'react-router';
import AddNode from '../components/AddNode';
import {connect} from "react-redux";
import * as nodeSelectors from "../store/node/reducer";
import * as nodeActions from "../store/node/actions";
import { BrowserRouter } from "react-router-dom";

class AddTopic extends Component {

    createNode(text, parent, isArgument) {
        this.props.dispatch(nodeActions.createNode(text, parent, isArgument));
    }

    render () {
        if (this.props.createdNode !== undefined) {
            this.props.history.goBack();
            this.props.dispatch(nodeActions.getParents(1));
        }
        return (
            <div className='container'>
                <AddNode createNode={this.createNode.bind(this)} isTopic={true} title="Новая тема"/>
            </div>
        );
    }
}

// which props do we want to inject, given the global store state?
function mapStateToProps(state) {
    return {
        createdNode: nodeSelectors.getCreatedNode(state)
    };
}

export default connect(mapStateToProps)(AddTopic);