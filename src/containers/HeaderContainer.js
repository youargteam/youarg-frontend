import React, {Component} from 'react'
import {connect} from 'react-redux'
import Header from '../components/Header'
import * as accountActions from '../store/account/actions'
import * as accountSelectors from '../store/account/reducer'

class HeaderContainer extends Component {

    componentDidMount() {
        this.props.dispatch(accountActions.getSelfAccount())
    }

    render() {
        return (
            <Header selfAccount={this.props.selfAccount}/>
        )
    }
}

function mapStateToProps(state) {
    return {
        selfAccount: accountSelectors.getSelfAccount(state)
    }
}

export default connect(mapStateToProps)(HeaderContainer)