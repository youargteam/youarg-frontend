import React, { Component } from 'react';
import { Button } from 'react-bootstrap';
import { connect } from 'react-redux';
import * as nodeSelectors from '../store/node/reducer';
import * as nodeActions from '../store/node/actions';
import RecursiveListView from '../components/RecursiveListView';
import NodeWrapper from './NodeWrapper';
import _ from 'lodash';
import { ReplyCreatedEvent } from './Reply'
import * as nodeActionTypes from "../store/node/actionTypes";

class TopicsScreen extends Component {

    componentDidMount() {
        this.updateData()
    }

    setImportance(node, value, path) {
        this.props.dispatch(nodeActions.setImportance(node, value, path))
    }

    deleteImportance(node, path) {
        this.props.dispatch(nodeActions.deleteImportance(node, path))
    }

    updateData() {
        this.props.dispatch({type: nodeActionTypes.CLEAR_TREE})
        const topicPK = this.props.match.params.id;
        this.props.dispatch(nodeActions.getTree(topicPK));
    }

    updateTree(nodePK, nextPage, path) {
        this.props.dispatch(nodeActions.getTree(nodePK, nextPage, path))
    }

    hasMore(children) {
        if (children === undefined || children.results === undefined) {
            return true
        }
        return children.results.length < children.count
    }

    render() {
        if (this.props.tree === undefined) {
            this.props.history.goBack()
            return null
        }

        return (
            <div className="container" ref={ref => this.container = ref}>
                { Object.values(this.props.tree).length === 0 ? null :
                    <div>
                        <NodeWrapper node={this.props.tree} isParent={true} treeMode={true}/>
                        { this.hasMore(this.props.tree.children) ?
                            <Button onClick={() => {
                                this.updateTree(this.props.tree.pk, this.props.tree.children.nextPage, "")
                            }}>Больше...</Button>
                            : null
                        }
                        <hr className="medium-margins"/>
                    </div>
                }
                <RecursiveListView
                    tree={this.props.tree}
                    childrenAttributePath={"children.results"}
                    renderRow={(row, path) => {
                        return (<div className="container">
                            <NodeWrapper path={path}
                                  node={row}
                                  isParent={false}
                                  setImportance={this.setImportance.bind(this)}
                                  deleteImportance={this.deleteImportance.bind(this)}
                            />
                            {this.hasMore(row.children) ?
                                <Button
                                    onClick={() => this.updateTree(row.pk, row.children ? row.children.nextPage : undefined, path)}>Больше...</Button>
                                : null
                            }
                        </div>);
                    }}
                />
            </div>
        );
    }
}

// which props do we want to inject, given the global store state?
function mapStateToProps(state) {
    return {
        tree: nodeSelectors.getTree(state)
    };
}

export default connect(mapStateToProps)(TopicsScreen);