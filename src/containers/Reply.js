import React, {Component} from 'react';
import AddNode from '../components/AddNode';
import Node from '../components/Node';
import * as nodeActions from "../store/node/actions";
import * as nodeSelectors from "../store/node/reducer";
import * as nodeActionTypes from "../store/node/actionTypes";
import {connect} from "react-redux";
import { Redirect } from 'react-router';

class Reply extends Component {

    componentDidMount() {
        this.props.dispatch(nodeActions.getNode(this.props.match.params.id))
    }

    createNode(text, parent, isArgument) {
        this.props.dispatch(nodeActions.createNode(text, parent, isArgument));
    }

    render () {
        const nodePK = this.props.match.params.id
        if (this.props.createdNode !== undefined) {
            const treeParentPK = this.props.tree.pk
            this.props.dispatch({type: nodeActionTypes.CLEAR_NODE_CREATED})
            this.props.history.goBack();
        }
        return (
            <div className='container'>
                {this.props.node === undefined ? null : <Node node={this.props.node} hideFooter={true} isParent={true}/>}
                <AddNode parent={nodePK} createNode={this.createNode.bind(this)} title="Ответ"/>
            </div>
        );
    }
}

// which props do we want to inject, given the global store state?
function mapStateToProps(state) {
    return {
        createdNode: nodeSelectors.getCreatedNode(state),
        node: nodeSelectors.getNode(state),
        tree: nodeSelectors.getTree(state)
    };
}

export default connect(mapStateToProps)(Reply);