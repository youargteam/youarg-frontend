import React from 'react';
import Modal from 'react-modal';
import { connect } from 'react-redux';
import * as commonSelectors from '../store/common/reducer';

const customStyles = {
    content : {
        top                   : '50%',
        left                  : '50%',
        right                 : 'auto',
        bottom                : 'auto',
        marginRight           : '-50%',
        transform             : 'translate(-50%, -50%)'
    }
};

class ModalError extends React.Component {
    constructor() {
        super();

        this.state = {
            modalIsOpen: false
        };

        this.openModal = this.openModal.bind(this);
        this.afterOpenModal = this.afterOpenModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
    }

    openModal() {
        this.setState({modalIsOpen: true});
    }

    afterOpenModal() {
        // references are now sync'd and can be accessed.
        this.subtitle.style.color = '#f00';
    }

    closeModal() {
        this.setState({modalIsOpen: false});
    }

    componentWillReceiveProps(nextProps) {
        this.setState({modalIsOpen: true});
    }

    render() {
        return (
            <div>
                <Modal
                    isOpen={this.state.modalIsOpen}
                    onAfterOpen={this.afterOpenModal}
                    onRequestClose={this.closeModal}
                    style={customStyles}
                    contentLabel="Example Modal"
                >

                    <h2 ref={subtitle => this.subtitle = subtitle}>Проблемка</h2>
                    <br/>
                    <div>{this.props.error.message}</div>
                    <br/>
                    <button onClick={this.closeModal}>закрыть</button>
                </Modal>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {error: commonSelectors.getError(state)}
}

export default connect(mapStateToProps)(ModalError)