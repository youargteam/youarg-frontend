import {Button} from 'react-bootstrap';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import InfiniteScroll from 'react-infinite-scroller';
import * as nodeSelectors from '../store/node/reducer';
import * as accountSelectors from '../store/account/reducer';
import * as nodeActions from '../store/node/actions';
import ListView from '../components/ListView';
import NodeWrapper from './NodeWrapper';

class TopicsScreen extends Component {

    componentDidMount() {
        this.loadItems(1)
    }

    loadItems(page) {
        this.props.dispatch(nodeActions.getParents(page));
    }

    render() {
        return (
            <div className="container">
                {this.props.selfAccount === undefined ? null
                    : <Button bsStyle='primary' href='/add-topic/'>Добавить ноду</Button>
                }
                <div style={{height: 10}}/>
                <InfiniteScroll
                    loadMore={() => this.loadItems(this.props.nextPage)}
                    hasMore={this.props.hasMore}
                    loader={<p key="loading">"Загрузка"</p>}>
                    <ListView noPadding={true}
                        rows={this.props.parents}
                        renderRow={(row) => {
                            return <NodeWrapper node={row} isParent={true}/>
                        }}
                    />
                </InfiniteScroll>
            </div>
        );
    }
}

// which props do we want to inject, given the global store state?
function mapStateToProps(state) {
    return {
        parents: nodeSelectors.getParents(state),
        hasMore: nodeSelectors.getHasMoreParents(state),
        nextPage: nodeSelectors.getParentsNextPage(state),
        selfAccount: accountSelectors.getSelfAccount(state)
    };
}

export default connect(mapStateToProps)(TopicsScreen);