import React, {Component} from 'react';
import AddNode from '../components/AddNode';
import Node from '../components/Node';
import * as nodeActions from "../store/node/actions";
import * as nodeSelectors from "../store/node/reducer";
import * as nodeActionTypes from "../store/node/actionTypes";
import server from "../services/server";
import {connect} from "react-redux";
import { Redirect } from 'react-router';

class Edit extends Component {
    state = {
        node: undefined,
        parent: undefined
    }

    async componentDidMount() {
        var response = await server.getNode(this.props.match.params.id)
        const node = response.data
        if (node === undefined)
            return
        this.setState({
            node: node
        })
        response = await server.getNode(node.parent)
        this.setState({
            parent: response.data
        })
    }

    createNode(text, parent, isArgument) {
        this.props.dispatch(nodeActions.editNode(this.props.match.params.id, text, parent, isArgument));
    }

    render () {
        const node = this.state.node
        if (this.props.createdNode !== undefined) {
            this.props.dispatch({type: nodeActionTypes.CLEAR_NODE_CREATED})
            this.props.history.goBack();
        }
        return (
            <div className='container'>
                {this.state.parent === undefined ? null : <Node node={this.state.parent} hideFooter={true} isParent={true}/>}
                {this.state.node === undefined ? null
                    : <AddNode
                        parent={this.state.node.parent}
                        createNode={this.createNode.bind(this)}
                        text={this.state.node.text}
                        isArgument={this.state.node.argument}
                        title="Редактировать"
                        isTopic={this.state.node.parent === null}/>
                }
            </div>
        );
    }
}

// which props do we want to inject, given the global store state?
function mapStateToProps(state) {
    return {
        createdNode: nodeSelectors.getCreatedNode(state)
    };
}

export default connect(mapStateToProps)(Edit);