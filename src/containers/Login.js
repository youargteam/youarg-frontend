import React, { Component } from 'react';
import { Col, Row } from 'react-bootstrap';
import {connect} from 'react-redux';
import { serverBaseURL } from "../services/server";
import UserDefaults from "../services/userDefaults";
import getParamFromUrl from "../helpers/urlParams";
import { Redirect } from 'react-router';
import * as accountActions from '../store/account/actions';
import * as accountSelectors from '../store/account/reducer';

class Login extends Component {

    componentDidMount () {
        const token = getParamFromUrl('token');
        if (token !== null) {
            UserDefaults.setToken(token);
            this.props.dispatch(accountActions.getSelfAccount());
        }
    }

    render () {
        if (this.props.selfAccount !== undefined) {
            this.props.history.goBack()
        }

        return (
            <div className="container">
                <Row>
                    <Col sm={4} smOffset={4} className="social-buttons align-items-center">
                        <h3 align="center">Вход и регистрация</h3>
                        <a className="btn btn-block btn-social btn-facebook" href={serverBaseURL + 'facebook/login'}>
                            <span className="fab fa-facebook">
                            </span>
                            Войти с Facebook
                        </a>
                        <a className="btn btn-block btn-social btn-vk" href={serverBaseURL + 'vk/login'}>
                            <span className="fab fa-vk">
                            </span>
                            Войти с Vkontakte
                        </a>
                    </Col>
                </Row>
            </div>
        )
    }
}

function matchStateToProps(state) {
    return {
        selfAccount: accountSelectors.getSelfAccount(state)
    }
}

export default connect(matchStateToProps)(Login)