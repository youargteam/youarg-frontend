import React, {Component} from 'react';
import { Redirect } from 'react-router';
import {connect} from 'react-redux';
import UserDefaults from '../services/userDefaults';
import * as accountActions from '../store/account/actions';
import * as accountSelectors from '../store/account/reducer';


class Logout extends Component {
    componentDidMount() {
        UserDefaults.clearToken();
        this.props.dispatch(accountActions.getSelfAccount());
    }

    render() {
        if (this.props.selfAccount === undefined) {
            this.props.history.goBack();
        }

        return null;
    }
}

function matchStateToProps(state) {
    return {
        selfAccount: accountSelectors.getSelfAccount(state)
    }
}

export default connect(matchStateToProps)(Logout)